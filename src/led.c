#include "led.h"

#include <errno.h>


FILE *led_open(const char *name) {
    // open the sysfs brightness file
    char filename[256] = {0};
    int count = snprintf(filename, sizeof(filename),
        "/sys/class/leds/%s/brightness", name);

    // LED name is too large to put into path template
    if (count < 0 || count >= sizeof(filename)) {
        errno = ENAMETOOLONG;
        return NULL;
    }

    FILE *led_file = fopen(filename, "w");
    if (led_file == NULL) {
        return NULL;
    }

    // set to unbuffered so we can write single bytes with less calls
    setbuf(led_file, NULL);

    return led_file;
}

int led_set(FILE *led_file, uint8_t on) {
    char on_char = on ? '1' : '0';
    if (fputc(on_char, led_file) == EOF) {
        return -1;
    }
    return 0;
}
