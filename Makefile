##
# Multi-binary Makefile
# Sources files in `source_dir` are compiled to objects in `object_dir`,
# then linked and placed in `binary_dir`.
#
# Commands:
#     all: Build all programs
#     clean: Remove all generated files
#     <program>: Build a single program and place in `binary_dir`


##
# Options
#

override CFLAGS := -std=c11 -D_POSIX_C_SOURCE=200809L -pedantic -Wall -Werror $(CFLAGS)
#LDLIBS =


##
# Vars / Helpers
#

# all available programs, match to <progname>.c
programs = candle cave

# build locations
source_dir = ./src
object_dir = ./obj
binary_dir = ./bin

# build artifacts
main_sources := $(programs:%=$(source_dir)/%.c)
dep_sources := $(filter-out $(main_sources),$(wildcard $(source_dir)/*.c))
dep_objects := $(dep_sources:$(source_dir)/%.c=$(object_dir)/%.o)


##
# Rules
#

# generate everything
.PHONY: all
all: $(programs)

# remove generated objects
.PHONY: clean
clean:
	rm -rf $(object_dir) $(binary_dir)

# map program names to binaries
.PHONY: $(programs)
$(programs): %: $(binary_dir)/%

# create directories
$(object_dir) $(binary_dir):
	mkdir -p $@

# assemble a binary
$(binary_dir)/%: $(object_dir)/%.o $(dep_objects) | $(binary_dir)
	$(CC) $^ $(LDFLAGS) $(LDLIBS) -o $@

# compile a source file
$(object_dir)/%.o:: $(source_dir)/%.c | $(object_dir)
	$(CC) $(CFLAGS) -c $< -o $@
