/**
 * First in last out structure
 */

#ifndef STACK_H
#define STACK_H

#include <stddef.h>


// stack of generic pointers
struct Stack {
    void **items; // array of pointers
    size_t count; // count of items
    size_t size; // max count of items
};

// allocate stack
int stack_init(struct Stack *stack, size_t size);

// de-allocate stack
void stack_destroy(struct Stack *stack);

// push an item onto stack
int stack_push(struct Stack *stack, void *item);

// pop an item off stack
void *stack_pop(struct Stack *stack);

// look at top of stack without modifying it
void *stack_peek(struct Stack *stack);


#endif /* STACK_H */
