/**
 * Simple sysfs LED driver that controls brightness
 */

#ifndef LED_H
#define LED_H

#include <stdint.h>
#include <stdio.h>


// open /sys/class/leds/<name>/brightness for unbuffered writing
FILE *led_open(const char *name);

// set LED state, 0 = off, 1 = on
int led_set(FILE *led_file, uint8_t on);


#endif /* LED_H */
