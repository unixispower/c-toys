#include "stack.h"

#include <stdlib.h>


int stack_init(struct Stack *stack, size_t size) {
    stack->items = malloc(size * sizeof(void *));
    if (stack->items == NULL) {
        return -1;
    }
    stack->count = 0;
    stack->size = size;
    return 0;
}

void stack_destroy(struct Stack *stack) {
    free(stack->items);
    stack->items = NULL;
    stack->count = 0;
    stack->size = 0;
}

int stack_push(struct Stack *stack, void *item) {
    // stack is full
    if (stack->count == stack->size) {
        return -1;
    }

    stack->items[stack->count] = item;
    ++stack->count;
    return 0;
}

void *stack_pop(struct Stack *stack) {
    // stack is empty
    if (stack->count == 0) {
        return NULL;
    }

    return stack->items[--stack->count];
}

void *stack_peek(struct Stack *stack) {
    // stack is empty
    if (stack->count == 0) {
        return NULL;
    }

    return stack->items[stack->count - 1];
}
