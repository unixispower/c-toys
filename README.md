# Blaine's C Toys
This repo contains some "toy" programs written in C.


## Programs
Contained herein are standalone programs targeting Linux (some utilize
Linux-specific features, others may be more portable).

### candle
This is a userspace LED PWM driver that twiddles
[sysfs entries](https://docs.kernel.org/leds/leds-class.html) to simulate the
flicker of a candle. See [candle.c](src/candle.c).

### cave
This is a 2d maze generator that produces maps that resemble caves. See
[cave.c](src/cave.c).


## Building
Everything is built by a single `Makefile` using a C compiler. On my machine
this equates to GNU Make and GCC.

```shell
# to build a single program
make <program>

# to build everything
make all

# to clean up
make clean
```

Programs are placed into the `bin` directory.


## Licensing
Source in this repository is licensed under the 2-clause BSD license, see
`LICENSE` for details.
