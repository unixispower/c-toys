/**
 * Cave generator.
 * This program generates a maze that resembles a cave and then prints it to
 * stdout.
 */

#include "input.h"
#include "pane.h"
#include "stack.h"
#include "util.h"

#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>


static const char * const USAGE =
    "Usage: cave [options]\n"
    "Options\n"
    "    -w <width>     Number of horizontal cells, defaults to 24\n"
    "    -h <height>    Number of vertical cells, defaults to 24\n"
    "    -c <charset>   Character set to use when printing:\n"
    "                   ascii   ASCII (default)\n"
    "                   u8b     UTF-8 block\n"
    "                   u8bw    UTF-8 block wide\n"
    "                   cp437b  IBM code page 437 block\n";


static const uint8_t MIN_SIZE = 4; // smaller doesn't leave room to work in
static const uint8_t MAX_SIZE = UINT8_MAX;
static const uint8_t DEFAULT_SIZE = 24;

// characters used in the pane
static const char WALL_CHAR = '#';
static const char PATH_CHAR = ' ';
static const char START_CHAR = '@';
static const char END_CHAR = '$';


// generate cave on pane
static int generate_cave(struct Pane *pane) {
    // fill pane with wall and "dig" out paths later
    pane_fill(pane, WALL_CHAR);

    // start in the center
    char *start_cell = pane_get_cell(pane, pane->width / 2,  pane->height / 2);
    *start_cell = PATH_CHAR;

    // end point is eventually set to longest path from starting point
    uint16_t max_stack_count = 0;
    char *end_cell = start_cell;

    // store traversal path in a stack
    struct Stack path_stack;
    if (stack_init(&path_stack, (pane->width * pane->height) / 2) != 0) {
        return -1;
    }

    // "dig" a path by searching wall space
    stack_push(&path_stack, start_cell);
    while (path_stack.count > 0) {
        // set end point to point with maximum stack depth
        if (path_stack.count > max_stack_count) {
            max_stack_count = path_stack.count;
            end_cell = stack_peek(&path_stack);
        }

        // visit next cell
        char *current_cell = stack_pop(&path_stack);
        *current_cell = PATH_CHAR;

        // translate the cell into pane coordinates
        uint8_t x;
        uint8_t y;
        pane_get_xy(pane, current_cell, &x, &y);

        // build a list of next possible cells
        char *safe_cells[4];
        size_t safe_cell_count = 0;

        // check if it's safe to dig north
        //  XXX
        //  X?X
        //   @
        if (y > 1 && x >= 1 && x < pane->width - 1) {
            char *north_cell = pane_get_cell(pane, x, y - 1);
            if (*north_cell == WALL_CHAR                                // n
                    && *pane_get_cell(pane, x - 1, y - 1) == WALL_CHAR  // n w
                    && *pane_get_cell(pane, x - 1, y - 2) == WALL_CHAR  // n nw
                    && *pane_get_cell(pane, x,     y - 2) == WALL_CHAR  // n n
                    && *pane_get_cell(pane, x + 1, y - 2) == WALL_CHAR  // n ne
                    && *pane_get_cell(pane, x + 1, y - 1) == WALL_CHAR) { // n e
                safe_cells[safe_cell_count++] = north_cell;
            }
        }

        // check if it's safe to dig east
        //   XX
        //  @?X
        //   XX
        if (x < pane->width - 2 && y >= 1 && y < pane->height - 1) {
            char *east_cell = pane_get_cell(pane, x + 1, y);
            if (*east_cell == WALL_CHAR                                 // e
                    && *pane_get_cell(pane, x + 1, y - 1) == WALL_CHAR  // e n
                    && *pane_get_cell(pane, x + 2, y - 1) == WALL_CHAR  // e ne
                    && *pane_get_cell(pane, x + 2, y)     == WALL_CHAR  // e e
                    && *pane_get_cell(pane, x + 2, y + 1) == WALL_CHAR  // e se
                    && *pane_get_cell(pane, x + 1, y + 1) == WALL_CHAR) { // e s
                safe_cells[safe_cell_count++] = east_cell;
            }
        }

        // check if it's safe to dig south
        //   @
        //  X?X
        //  XXX
        if (y < pane->height - 2 && x >= 1 && x < pane->width - 1) {
            char *south_cell = pane_get_cell(pane, x, y + 1);
            if (*south_cell == WALL_CHAR                                // s
                    && *pane_get_cell(pane, x + 1, y + 1) == WALL_CHAR  // s e
                    && *pane_get_cell(pane, x + 1, y + 2) == WALL_CHAR  // s se
                    && *pane_get_cell(pane, x,     y + 2) == WALL_CHAR  // s s
                    && *pane_get_cell(pane, x - 1, y + 2) == WALL_CHAR  // s sw
                    && *pane_get_cell(pane, x - 1, y + 1) == WALL_CHAR) { // s w
                safe_cells[safe_cell_count++] = south_cell;
            }
        }

        // check if it's safe to dig west
        //  XX
        //  X?@
        //  XX
        if (x > 1 && y >= 1 && y < pane->height - 1) {
            char *west_cell = pane_get_cell(pane, x - 1, y);
            if (*west_cell == WALL_CHAR                                 // w
                    && *pane_get_cell(pane, x - 1, y + 1) == WALL_CHAR  // w s
                    && *pane_get_cell(pane, x - 2, y + 1) == WALL_CHAR  // w sw
                    && *pane_get_cell(pane, x - 2, y)     == WALL_CHAR  // w w
                    && *pane_get_cell(pane, x - 2, y - 1) == WALL_CHAR  // w nw
                    && *pane_get_cell(pane, x - 1, y - 1) == WALL_CHAR) { // w n
                safe_cells[safe_cell_count++] = west_cell;
            }
        }

        // choose a one of the safe cells as the next point
        if (safe_cell_count > 0) {
            // place current cell back on the stack
            stack_push(&path_stack, current_cell);

            // choose a random cell to visit next
            char *next_cell = safe_cells[rand() % safe_cell_count];
            stack_push(&path_stack, next_cell);
        }
    }

    // mark start and end
    *start_cell = START_CHAR;
    *end_cell = END_CHAR;

    stack_destroy(&path_stack);
    return 0;
}

// charset u8b: utf8 block
const char *map_to_u8b(char pane_char) {
    if      (pane_char == WALL_CHAR)    { return "\xE2\x96\x88"; }
    return NULL;
}

// charset cp437: IBM code page 437 block
const char *map_to_cp437b(char pane_char) {
    if      (pane_char == WALL_CHAR)    { return "\xDB"; }
    return NULL;
}

// charset u8bw: utf8 block wide
const char *map_to_u8bw(char pane_char) {
    if      (pane_char == WALL_CHAR)    { return "\xE2\x96\x88\xE2\x96\x88"; }
    else if (pane_char == PATH_CHAR)    { return "  "; }
    else if (pane_char == START_CHAR)   { return "\xEF\xBC\xA0"; }
    else if (pane_char == END_CHAR)     { return "\xEF\xBC\x84"; }
    return NULL;
}

// parse and validate command line arguments
int parse_args(int argc, char *argv[],
        uint8_t *width, uint8_t *height, const char *(**map_char)(char)) {
    int option;
    opterr = 0;
    while ((option = getopt(argc, argv, ":w:h:c:")) != -1) {
        long temp = 0;

        switch(option) {
            // width
            case 'w':
                if (parse_long(optarg, "width",
                        MIN_SIZE, MAX_SIZE, &temp) != 0) {
                    return -1;
                }
                *width = temp;
                break;

            // height
            case 'h':
                if (parse_long(optarg, "height",
                        MIN_SIZE, MAX_SIZE, &temp) != 0) {
                    return -1;
                }
                *height = temp;
                break;

            // charset
            case 'c':
                if (strcmp(optarg, "ascii") == 0) {
                    *map_char = NULL;
                } else if (strcmp(optarg, "u8b") == 0) {
                    *map_char = map_to_u8b;
                } else if (strcmp(optarg, "u8bw") == 0) {
                    *map_char = map_to_u8bw;
                } else if (strcmp(optarg, "cp437b") == 0) {
                    *map_char = map_to_cp437b;
                } else {
                    fprintf(stderr, "Invalid charset \"%s\"\n", optarg);
                    return -1;
                }
                break;

            // missing option value
            case ':':
                fprintf(stderr, "Option -%c requires a value\n", optopt);
                return -1;

            // unknown option
            case '?':
                fprintf(stderr, "Unrecognized option \"-%c\"\n", optopt);
                return -1;
        }
    }

    // positional arguments are not accepted
    if (optind != argc) {
        fprintf(stderr, "Unexpected argument \"%s\"\n", argv[optind]);
        return -1;
    }

    return 0;
}

int main(int argc, char *argv[]) {
    uint8_t width = DEFAULT_SIZE;
    uint8_t height = DEFAULT_SIZE;
    const char *(*map_char_func)(char) = NULL;
    if (parse_args(argc, argv, &width, &height, &map_char_func) != 0) {
        fprintf(stderr, USAGE);
        return EXIT_FAILURE;
    }

    rand_init();

    struct Pane pane;
    if (pane_init(&pane, width, height) != 0) {
        fprintf(stderr, "Failed to initialize pane: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    if (generate_cave(&pane) != 0) {
        fprintf(stderr, "Failed to generate cave: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    if (map_char_func != NULL) {
        pane_map_print(&pane, map_char_func);
    } else {
        pane_print(&pane);
    }
    pane_destroy(&pane);

    return EXIT_SUCCESS;
}
