/**
 * Linux LED "candle" simulator.
 * This program drives a /sys/class/leds entry with a low frequency "PWM"
 * waveform to simulate a flickering candle. PWM is faked in userspace by
 * toggling the state of the `brightness` file very quickly (blinking at 100Hz
 * by default).
 */

#include "input.h"
#include "led.h"
#include "util.h"

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>


static const char * const USAGE =
    "Usage: candle [options] <led>\n"
    "Parameters\n"
    "    led            Name of LED directory (/sys/class/leds/<name>/)\n"
    "Options\n"
    "    -f <pwm_freq>  PWM frequency in Hz [1,1000], defaults to 100\n"
    "    -l <min_duty>  Minimum PWM duty cycle [0,100], defaults to 1\n"
    "    -h <max_duty>  Maximum PWM duty cycle [1,100], defaults to 100\n"
    "    -i <periods>   Periods to interpolate for [1,255], defaults to 10\n";


static volatile sig_atomic_t running = 1;

// set running to 0 on SIGINT
static void handle_sigint(int signo) {
    running = 0;
    signal(signo, handle_sigint);
}

// simulate 1 PWM "pulse" by toggling the LED state and delaying
static int pulse_led(FILE *led_file,
        uint32_t pwm_period_nsec, float duty_cycle) {
    struct timespec sleep_time;
    sleep_time.tv_sec = 0;

    uint32_t on_nsec = duty_cycle * pwm_period_nsec;
    if (on_nsec > 0) {
        if (led_set(led_file, 1) != 0) {
            return -1;
        }
        sleep_time.tv_nsec = on_nsec;
        nanosleep(&sleep_time, NULL);
    }

    uint32_t off_nsec = pwm_period_nsec - on_nsec;
    if (off_nsec > 0) {
        if (led_set(led_file, 0) != 0) {
            return -1;
        }
        sleep_time.tv_nsec = off_nsec;
        nanosleep(&sleep_time, NULL);
    }

    return 0;
}

// parse and validate command line arguments
int parse_args(int argc, char *argv[],
        const char **led_name, uint32_t *pwm_period_nsec,
        float *min_duty_cycle, float *max_duty_cycle, uint8_t *inter_periods) {
    *led_name = NULL;
    *pwm_period_nsec = 10 * 1000 * 1000; // 10ms (100Hz)
    *min_duty_cycle = 0.1f; // 10%
    *max_duty_cycle = 1.0f; // 100%
    *inter_periods = 10; // 10 periods * 10ms = 100ms

    opterr = 0;
    while (optind < argc) {
        // handle option argument
        int option;
        if ((option = getopt(argc, argv, ":f:l:h:i:")) != -1) {
            long temp = 0;
            switch(option) {
                // PWM frequency
                case 'f':
                    if (parse_long(optarg, "pwm_freq",
                        1, 1000, &temp) != 0) {
                        return -1;
                    }
                    *pwm_period_nsec = 1000000000L / temp;
                    break;

                // minimum PWM duty cycle
                case 'l':
                    if (parse_long(optarg, "min_duty",
                            0, 100, &temp) != 0) {
                        return -1;
                    }
                    *min_duty_cycle = temp / 100.0f;
                    break;

                // maximum PWM duty cycle
                case 'h':
                    if (parse_long(optarg, "max_duty",
                            1, 100, &temp) != 0) {
                        return -1;
                    }
                    *max_duty_cycle = temp / 100.0f;
                    break;

                // interpolation periods
                case 'i':
                    if (parse_long(optarg, "periods",
                            1, UINT8_MAX, &temp) != 0) {
                        return -1;
                    }
                    *inter_periods = temp;
                    break;

                // missing option value
                case ':':
                    fprintf(stderr, "Option -%c requires a value\n", optopt);
                    return -1;

                // unknown option
                case '?':
                    fprintf(stderr, "Unrecognized option: -%c\n", optopt);
                    return -1;
            }

        // handle positional argument
        } else {
            // allow a single positional argument
            if (*led_name != NULL) {
                fprintf(stderr, "Unrecognized argument: %s\n", argv[optind]);
                return -1;
            }

            *led_name = argv[optind];
            ++optind;
        }
    }

    // require LED name
    if (*led_name == NULL) {
        fprintf(stderr, "LED name is required\n");
        return -1;
    }

    // ensure min is less than max
    if (*min_duty_cycle >= *max_duty_cycle) {
        fprintf(stderr,
            "Minimum duty cycle must be less than maximum duty cycle\n");
        return -1;
    }

    return 0;
}

int main(int argc, char *argv[]) {
    const char *led_name;
    uint32_t pwm_period_nsec;
    float min_duty_cycle;
    float max_duty_cycle;
    uint8_t inter_periods;

    if (parse_args(argc, argv, &led_name, &pwm_period_nsec,
            &min_duty_cycle, &max_duty_cycle, &inter_periods) != 0) {
        fprintf(stderr, USAGE);
        return EXIT_FAILURE;
    }

    // gracefully exit on SIGINT
    if (signal(SIGINT, handle_sigint) == SIG_ERR) {
        fprintf(stderr,
            "An error occurred while registering a signal handler: %s\n",
            strerror(errno));
        return EXIT_FAILURE;
    }

    // open LED file
    FILE *led_file = led_open(led_name);
    if (led_file == NULL) {
        fprintf(stderr,
            "Failed to open LED %s: %s\n", led_name, strerror(errno));
        return EXIT_FAILURE;
    }

    rand_init();

    // flicker until shutdown
    float start_duty_cycle = 0;
    float end_duty_cycle = 0;
    while (running) {
        // pick a new duty cycle to change to
        end_duty_cycle = rand_float(min_duty_cycle, max_duty_cycle);

        // interpolate between start_duty_cycle and end_duty_cycle
        for (uint8_t i = 0; i < inter_periods; ++i) {
            if (!running) {
                break;
            }
            float progress = (float)i / (float)inter_periods;
            float duy_cycle = lerp(start_duty_cycle, end_duty_cycle, progress);
            if (pulse_led(led_file, pwm_period_nsec, duy_cycle) != 0) {
                fprintf(stderr,
                    "Failed to set LED %s: %s\n", led_name, strerror(errno));
                fclose(led_file);
                return EXIT_FAILURE;
            }
        }

        // set ending duty cycle as the new starting point
        start_duty_cycle = end_duty_cycle;
    }

    led_set(led_file, 0);
    fclose(led_file);
    return EXIT_SUCCESS;
}
