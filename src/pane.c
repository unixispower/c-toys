#include "pane.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int pane_init(struct Pane *pane, uint8_t width, uint8_t height) {
    size_t data_size = width * height;
    pane->width = width;
    pane->height = height;

    pane->data = malloc(data_size);
    if (pane->data == NULL) {
        return -1;
    }
    memset(pane->data, '\0', data_size);

    return 0;
}

void pane_destroy(struct Pane *pane) {
    free(pane->data);
    pane->data = NULL;
    pane->width = 0;
    pane->height = 0;
}

char *pane_get_cell(struct Pane *pane, uint8_t x, uint8_t y) {
    // cell is out of bounds
    if (x >= pane->width || y >= pane->height) {
        return NULL;
    }
    size_t offset = y * pane->width + x;
    return pane->data + offset;
}

int pane_get_xy(struct Pane *pane, char *cell, uint8_t *x, uint8_t *y) {
    // cell is not in pane
    size_t data_size = pane->width * pane->height;
    if (cell < pane->data || cell >= pane->data + data_size) {
        return -1;
    }
    size_t offset = cell - pane->data;
    *x = offset % pane->width;
    *y = offset / pane->width;
    return 0;
}

void pane_fill(struct Pane *pane, char value) {
    size_t data_size = pane->width * pane->height;
    memset(pane->data, value, data_size);
}

void pane_print(struct Pane *pane) {
    for (uint8_t y = 0; y < pane->height; ++y) {
        char *row = pane_get_cell(pane, 0, y);
        fwrite(row, sizeof(char), pane->width, stdout);
        fputc('\n', stdout);
    }
    fflush(stdout);
}

void pane_map_print(struct Pane *pane, const char *(*map_char)(char)) {
    const char *newline = map_char('\n');

    for (uint8_t y = 0; y < pane->height; ++y) {
        char *row = pane_get_cell(pane, 0, y);
        for (uint8_t x = 0; x < pane->width; ++x) {
            const char original_char = row[x];
            const char *mapped = map_char(original_char);
            if (mapped != NULL) {
                fputs(mapped, stdout);
            } else {
                fputc(original_char, stdout);
            }
        }
        if (newline != NULL) {
            fputs(newline, stdout);
        } else {
            fputc('\n', stdout);
        }
    }

    fflush(stdout);
}
