/**
 * A 2D grid of characters
 */

#ifndef PANE_H
#define PANE_H

#include <stdint.h>


// 2d grid of characters that can be printed to a stream
struct Pane {
    char *data; // consecutive rows, each "width" long
    uint8_t width; // number of (vertical) columns
    uint8_t height; // number of (horizontal) rows
};

// allocate the memory for pane
int pane_init(struct Pane *pane, uint8_t width, uint8_t height);

// de-allocate the memory for pane
void pane_destroy(struct Pane *pane);

// get pointer to cell
char *pane_get_cell(struct Pane *pane, uint8_t x, uint8_t y);

// get coordinates of cell
int pane_get_xy(struct Pane *pane, char *cell, uint8_t *x, uint8_t *y);

// fill pane with a specific character
void pane_fill(struct Pane *pane, char value);

// print pane to stdout
void pane_print(struct Pane *pane);

// print pane to stdout, but translate characters using a callback
void pane_map_print(struct Pane *pane, const char *(*map_char)(char));


#endif /* PANE_H */
