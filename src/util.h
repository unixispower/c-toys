/**
 * Miscellaneous utility functions
 */

#ifndef UTIL_H
#define UTIL_H


// call srand with a high resolution clock
void rand_init(void);

// return a random float in the range [min_value,max_value]
float rand_float(float min_value, float max_value);

// interpolate a value between start and end
float lerp(float start, float end, float progress);


#endif /* UTIL_H */
