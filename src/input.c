#include "input.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int parse_long(const char *input, const char *label,
        long min, long max, long *out_value) {
    errno = 0;
    char *end = NULL;
    long raw_value = strtol(input, &end, 10);

    if (input == end || *end != '\0') {
        fprintf(stderr, "Invalid %s \"%s\"\n", label, input);
        return -1;

    } else if (errno) {
        fprintf(stderr, "Failed to parse %s: %s\n",
                label, strerror(errno));
        return -1;

    } else if (raw_value < min || raw_value > max) {
        fprintf(stderr,
        "Value of %s must be in range [%ld,%ld]\n", label, min, max);
        return -1;
    }

    *out_value = raw_value;
    return 0;
}

int parse_float(const char *input, const char *label,
        float min, float max, float *out_value) {
    errno = 0;
    char *end = NULL;
    float raw_value = strtof(input, &end);

    if (input == end || *end != '\0') {
        fprintf(stderr, "Invalid %s \"%s\"\n", label, input);
        return -1;

    } else if (errno) {
        fprintf(stderr, "Failed to parse %s: %s\n",
                label, strerror(errno));
        return -1;

    } else if (raw_value < min || raw_value > max) {
        fprintf(stderr,
                "Value of %s must be in range [%.1f,%.1f]\n", label, min, max);
        return -1;
    }

    *out_value = raw_value;
    return 0;
}
