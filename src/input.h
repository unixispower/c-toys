/**
 * Input parsing helpers
 */

#ifndef INPUT_H
#define INPUT_H


// parse a long and validate it's within a certain range
int parse_long(const char *input, const char *label,
    long min, long max, long *out_value);

// parse a float and validate it's within a certain range
int parse_float(const char *input, const char *label,
    float min, float max, float *out_value);


#endif /* INPUT_H */
