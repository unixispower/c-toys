#include "util.h"

#include <stdlib.h>
#include <time.h>


void rand_init(void) {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    srand(now.tv_nsec);
}

float rand_float(float min_value, float max_value) {
    float multiplier = (float)rand() / (float)(RAND_MAX);
    float range = max_value - min_value;
    return multiplier * range + min_value;
}

float lerp(float start, float end, float progress) {
    return start + progress * (end - start);
}
